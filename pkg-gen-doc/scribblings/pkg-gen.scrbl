#lang scribble/manual

@(require (for-label pkg-gen))

@title{pkg-gen}

@author{promethea}

@defmodule[pkg-gen]

An automagical multi-package generator; like raco pkg new but for packages
containing pkg-lib, pkg-doc and pkg-test.